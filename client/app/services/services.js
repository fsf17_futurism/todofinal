//copy from Tong Fatt via slack
(function(){
    var Task = angular.module("Task", []);

    var TaskSvc = function($http){

        var taskSvc = this;

        //exposed the function
        taskSvc.retrieveTask = retrieveTask;
        taskSvc.deleteTask = deleteTask;
        taskSvc.insertTask = insertTask;
        taskSvc.fileTask = fileTask;


        function retrieveTask (){
        return ($http({
              method: 'GET'
            , url: 'api/tasks'
        })
            .then(function(result){    
            console.log(result);        //dept example does not have the .then function
            return(result.data);
            }));
        };
        
        function insertTask(task){   //testing using GET to replace POST
            console.log(task);
            return $http({
                  method: 'POST'
                , url: 'api/tasks'
                , data:
                {
           task_id: task.task_id,
           task_description: task.task_description,
           due_date: task.due_date,
           category: task.category,
           status: task.status}            
            });        
        };

        function deleteTask(task_id){    //
            return $http({
                    method: 'DELETE'
                    ,  url: 'api/tasks/'+ task_id
            });
        };

        function fileTask(update){            
            console.log("fileTask:", update);
            return $http({
                    method: 'PUT'
                    , url: 'api/tasks/'+ update.task_id
                    , data:{
                        task_id : update.task_id
                        , task_description : update.task_description
                        , category : update.category
                        , due_date : update.due_date
                        , status : update.status}
            });
        };
    }
    Task.service('TaskSvc',['$http', TaskSvc])
})();