//Main codes were copied from TongFatt via slack

(function(){
    var TaskApp = angular.module("TaskApp", ["Task"]);
    var TaskCtrl=function(TaskSvc){
        var TaskCtrl=this;
        TaskCtrl.task_id="";
        TaskCtrl.task_description="";
        TaskCtrl.due_date=""; 
        TaskCtrl.category="";
        TaskCtrl.newCategory="";
        TaskCtrl.status="";
        TaskCtrl.array=[];
        TaskCtrl.categoryLabel=["Uncategorized", "Full Stack Foundation", "Personal Development", "Errands" ];
        TaskCtrl.filter="";
// populate with sample data 
        /*TaskCtrl.array = [  {id: 1,task:"Watch Youtube Video on Hair Styling",duedate:"18/02/2017", formattedDate:"18/02/2017", category:"Personal Development", status:"Pending" },
                            {id: 2,task:"Complete group assignment",duedate:"01/03/2017",formattedDate:"01/03/2017", category:"Full Stack Foundation",status:"Pending"  },
                            {id: 3,task:"Buy movie tickets",duedate:"01/04/2017", formattedDate:"01/04/2017",category:"Errands", status:"Pending" },
                            {id: 6,task:"Set up an in-kitchen hydroponics farm",duedate:"01/04/2017", formattedDate:"01/04/2017",category:"Errands", status:"Pending" },
                            {id: 5,task:"Buy tea for everyone",duedate:"01/04/2017", formattedDate:"01/04/2017",category:"Full Stack Foundation", status:"Pending" }
        ];*/
// adding a new task into the array of tasks
    
    //Initialize from existing tasks schemas database - codes from Beng Teck
        init();

        function init() {   
            console.log("Starting initialization");
            // We call Service.retrieveDeptDB to handle retrieval of department information. The data retrieved
            // from this function is used to populate search.html. Since we are initializing the view, we want to
            // display all available departments, thus we ask service to retrieve '' (i.e., match all)
            
             TaskSvc
                .retrieveTask()
                .then(function (results) {
                    //results returned by DB contains data object, which in turn contains the records //read from the database
                    TaskCtrl.array = results;
                    console.log("TaskCtrl.array " + JSON.stringify(TaskCtrl.array));
                    console.log("Retrieved 'tasks' Database Current Records")                  
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error " + err);
                });
            }; 

    
    TaskCtrl.add = function(){
            
            TaskCtrl.formattedDate = TaskCtrl.due_date //.toLocaleDateString('en-GB'); //to change date format
            //determine largest current id to generate the next id
            var aid = [];
            var maxid = 0
            for(i=0; i<TaskCtrl.array.length; i++){
                aid.push(parseInt(TaskCtrl.array[i].task_id));
                if (aid[i]>maxid) 
                        maxid = aid[i]     
                };
                TaskCtrl.task_id= maxid+1; 
             
            TaskCtrl.array.push( {
                task_id: parseInt(TaskCtrl.task_id),
                task_description:TaskCtrl.task_description, 
                category:TaskCtrl.category, 
                due_date:TaskCtrl.due_date, 
                formattedDate:TaskCtrl.formattedDate,
                status: "Pending",  
            } );
            console.log("TaskCtrl.task_id:" + TaskCtrl.task_id);
            TaskCtrl.addTask();
    };    
// removing a task from the array of tasks
        TaskCtrl.delete = function(idx){
            console.log("id:"+ TaskCtrl.array[idx].task_id);
            TaskCtrl.removeTask(TaskCtrl.array[idx].task_id);
            TaskCtrl.array.splice(idx,1);
        }
// save a task that has been edited
        TaskCtrl.save = function(idx){
      
        TaskCtrl.update = TaskCtrl.array[idx];
        console.log("save id:"+ TaskCtrl.update);
         TaskCtrl.saveTask();   
        }        
    // using findIndex of task matching for the 1st item in array will have problem when two or more tasks or same     
    // TaskCtrl.delete=function(placeholder){  
    //     var idx=TaskCtrl.array.findIndex(function(elem){
    //         return(placeholder == elem.task)
    //     })
    //     if (idx>=0){
    //         TaskCtrl.array.splice(idx,1);
    //     console.log(idx);
    //     }
         
    //         //splice(3,1,) means go to the 4th basket and remove 1, starting from it
    // }
//adding a new category
    TaskCtrl.addCategory=function(){
        TaskCtrl.categoryLabel.push(TaskCtrl.newCategory);
    console.log(TaskCtrl.categoryLabel);
}    
//adding a check function to mark completed Task
    TaskCtrl.check =function(idx){
        TaskCtrl.array[idx].status = "Done"
        console.log("idx : %s", idx );
        TaskCtrl.save(idx);
    }
// edit changes
TaskCtrl.newField = [];
    TaskCtrl.editing = false;
 TaskCtrl.editArray = function(idx) { 
        console.log(TaskCtrl.array[idx]);
        TaskCtrl.newField[idx] = angular.copy(TaskCtrl.array[idx]);
        }
// cancel changes in editing
   
    TaskCtrl.cancel = function(index) {
           TaskCtrl.array[index] = TaskCtrl.newField[index];
            TaskCtrl.editing = false;
        
    };
// http calls for refresh, insert and delete
    // TaskCtrl.array = [];                                                 placeholder
    
    //expose the functions
    TaskCtrl.refresh = refresh;
    TaskCtrl.addTask = addTask;
    TaskCtrl.saveTask = saveTask;
    TaskCtrl.removeTask = removeTask;
    TaskCtrl.result={};
 
    function refresh(){
            TaskCtrl.array=[];
        var promise = TaskSvc.retrieveTask()
            .then(function(allTasks){
                TaskCtrl.array=allTasks;     
                console.log("TaskCtrl.array:\n %s", JSON.stringify(TaskCtrl.array));                        // need to check if allTasks
            })
            .catch(function(err){
                console.log("error" + err);
            });
    }
    
        //update exising records
    function saveTask(){       
        TaskSvc
        .fileTask(TaskCtrl.update)
        .then(function(response){
            return console.log("updated");
            });           
    }
    function addTask(){
    TaskCtrl.TaskItem = {
                task_id: TaskCtrl.task_id,
                task_description:TaskCtrl.task_description, 
                category:TaskCtrl.category, 
                due_date:TaskCtrl.due_date, 
                formattedDate:TaskCtrl.formattedDate,
                status: "Pending" 
            } 
        TaskSvc
            .insertTask(TaskCtrl.TaskItem)
            .then(function(){
                return console.log("task inserted");
            })
    }
    function removeTask(taskID){
        TaskSvc
        .deleteTask(taskID)
        .then(function(){
            return console.log("deleted");
        })
    }
///the } below belongs to the controller
}
    TaskApp.controller("TaskCtrl", ["TaskSvc", TaskCtrl])
})();